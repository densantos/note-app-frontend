let notes = [];

document.getElementById('addNote').addEventListener('click', function() {
    const username = document.getElementById('username').value;
    const note = document.getElementById('note').value;

    if(username && note) {
        notes.push({ username, note });
        document.getElementById('username').value = '';
        document.getElementById('note').value = '';
        alert('Note added successfully.');
    } else {
        alert('Please fill in both your name and note.');
    }
});

document.getElementById('retrieveNotes').addEventListener('click', function() {
    const output = document.getElementById('output');
    output.innerHTML = '';
    
    notes.forEach(noteObj => {
        output.innerHTML += `<p><strong>${noteObj.username}</strong>: ${noteObj.note}</p>`;
    });
});
